<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Auth::routes();
Route::get("/login", "Auth\LoginController@index");
Route::get("/register", "Auth\RegisterController@index");
Route::post("/login", "Auth\LoginController@login");
Route::post("/register", "Auth\RegisterController@register");
Route::get("/logout", "Marketplace\ControllerCustomer@logout");

Route::get('/', function () {
    return view('welcome');
});

Route::get('/halo', function () {
    return "Halo Pengguna";
});

Route::get('/halo/{nama}/{salam}', function ($nama, $salam) {
    return "Halo $nama, Selamat $salam.";
});

Route::get('/sapa', 'CobaController@sapa');
Route::get('/sapa/{nama}/{sapa}', 'CobaController@sapaLengkap');

// Route::get('/kategori', 'KategoriController@index');
// Route::post('/kategori', 'KategoriController@submit');
// Route::post('kategori/edit/', 'KategoriController@edit');
// Route::get('/kategori/delete', 'KategoriController@deleteSession');

// Route::get('/produk', 'ProdukController@index');
// Route::post('/produk', 'ProdukController@submit');

Route::prefix('customer')->group(function(){
  Route::post('tambah', 'Marketplace\ControllerCustomer@tambahCustomer');
  Route::get('/', 'Marketplace\ControllerCustomer@index');
  Route::get('hapus/{id}', 'Marketplace\ControllerCustomer@hapus');
  Route::get('edit/{id}', 'Marketplace\ControllerCustomer@edit_customer');
  Route::put('{id}', 'Marketplace\ControllerCustomer@update');
});

Route::prefix('produk')->group(function(){
  Route::post('tambah', 'Marketplace\ControllerProduct@tambahProduct');
  Route::get('/', 'Marketplace\ControllerProduct@index');
  Route::get('{id}', 'Marketplace\ControllerProduct@detail');
  Route::get('/edit/{id}', 'Marketplace\ControllerProduct@edit_product');
  Route::put('{id}', 'Marketplace\ControllerProduct@update');
  Route::get('detail/{id}', 'Marketplace\ControllerProduct@detail');
  Route::get('hapus/{id}', 'Marketplace\ControllerProduct@hapus');
});

Route::prefix('kategori')->group(function(){
  Route::post('/', 'Marketplace\ControllerCategory@tambahCategory');
  Route::get('/', 'Marketplace\ControllerCategory@index');
  Route::get('hapus/{id}', 'Marketplace\ControllerCategory@hapus');
  Route::get('edit/{id}', 'Marketplace\ControllerCategory@edit_kategori');
  Route::put('{id}', 'Marketplace\ControllerCategory@update');
  Route::get('detail/{id}', 'Marketplace\ControllerCategory@detail');
});

Route::prefix('order')->group(function(){
  Route::get('/', 'Marketplace\ControllerOrder@index');
  Route::post('/', 'Marketplace\ControllerOrder@tambahOrder');
  Route::get('detail/{id}', 'Marketplace\ControllerOrder@detailOrder');
  Route::get('hapus/{id}', 'Marketplace\ControllerOrder@hapusOrder');
});

Route::post('/orderDetail/{id}', 'Marketplace\ControllerOrder@tambahProduct');
