<?php

use Illuminate\Http\Request;
use App\Http\Resources\ProductsResources;
use App\Http\Models\Products;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

  Route::post('register','Auth\RegisterController@register');
  Route::post('login','Auth\LoginController@login');
  Route::get('logout','Auth\LoginController@logout');

  Route::group(['middleware' => 'auth:api'], function () {
  Route::get('user','Auth\UserController@detail');

  Route::prefix('v1')->group(function(){
    Route::prefix('belajar')->group(function(){
      Route::get('cobaget','API\V1\ApiController@method_get');
      Route::get('sapa','CobaController@index');
      Route::post('cobapost','API\V1\ApiController@method_post');
    });

    Route::prefix('kategori')->group(function(){
      Route::get('/','Marketplace\ControllerCategory@getApi');
      Route::get('{id}','Marketplace\ControllerCategory@detailApi');
      Route::post('/','Marketplace\ControllerCategory@addApi');
      Route::put('{id}','Marketplace\ControllerCategory@updateApi');
    });

    // Route::prefix('customer')->group(function(){
    //   Route::get('/','Marketplace\ControllerCustomer@getApi');
    //   Route::get('{id}','Marketplace\ControllerCustomer@detailApi');
    //   Route::post('/','Marketplace\ControllerCustomer@addApi');
    //   Route::put('{id}','Marketplace\ControllerCustomer@updateApi');
    // });

    Route::apiResource("order", "API\V1\OrderController");
    Route::apiResource("order_detail", "API\V1\OrderDetailController");
    Route::post("order_detail/{id}", "API\V1\OrderDetailController@store");
      Route::apiResource("customer", "API\V1\CustomerController");

  });

  Route::apiResource("kategori", "API\V1\CategoriesController");
  Route::apiResource("product", "API\V1\ProductController");

  });

//   Route::get('/products/{id}', function(Products $id) {
//      return ApiBuilder::apiRespond(200, ProductsResources::collection(Products::with('category')->find($id)));
// });
