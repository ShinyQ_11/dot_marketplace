<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Http\Models\Customers;
use Faker\Generator as Faker;

$factory->define(Customers::class, function (Faker $faker) {
  return [
      'email' => $faker->email,
      'first_name' => $faker->firstNameMale,
      'last_name' => $faker->lastName,
      'address' => $faker->streetAddress,
      'phone_number' => $faker->postcode,
      'password' => $faker->password
  ];
});
