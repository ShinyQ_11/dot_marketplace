<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Illuminate\Support\Str;
use App\Http\Models\Products;
use App\Http\Models\Categories;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

// $factory->define(Products::class, function (Faker $faker) {
//     $word  = $faker->word;
//     return [
//       'nama' => $word,
//       'unit_price' => rand(500,500000),
//       'category_id'=>function(){
//           return Categories::all()->random();
//       },
//
//     ];
// });

$factory->define(Products::class, function (Faker $faker) {

    return [
        //
        'nama' => $faker->word,
        'unit_price' => $faker->biasedNumberBetween($min = 10000, $max = 500000, $function = 'sqrt')
    ];
});
