<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;
use App\Http\Models\Categories;
use App\Http\Models\Products;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return
     */
    public function run()
    {
      $category = Categories::latest()->first();
      factory(Products::class,3)->create([
           'category_id'=> $category->id
       ]);

    }
}
