<?php

use Illuminate\Database\Seeder;
use App\Http\Models\Customers;

class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Customers::class,2)->create();
    }
}
