<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductsResources extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

      return [
          'nama' => $this->nama,
          'harga' => $this->unit_price,
          'gambar' => "http://localhost:8000/images/$this->image",
          'id' => $this->id,
          'kategori' => ['id' => $this->category_id, 'nama' =>  $this->category->nama],
          


      ];

      // return[
      //   'data' => $this->collection
      // ];
    }
}
