<?php

namespace App\Http\Models;
use App\Http\Models\Customers;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Orders extends Model
{
  protected $table = 'orders';
  protected $fillable = ['customer_id','total'];

    public function customer() {
    	return $this->hasOne(customers::class, "id", "customer_id");
    }

    public function order_detail() {
      return $this->hasMany(Orders_detail::class, "order_id", "id");
    }

}
