<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Orders_detail extends Model
{
  protected $table = 'orders_detail';

  protected $fillable = ['order_id','product_id','product_count','quantity','price'];

  function order(){
    return $this->hasOne(Orders::class, "id", "order_id");
  }

  function product(){
    return $this->hasOne(Products::class, "id", "product_id");
  }
}
