<?php

namespace App\Http\Models;
use App\Http\Models\Categories;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Categories extends Model
{
   use SoftDeletes;
   public $timestamps = true;
   protected $table = 'categories';
   protected $fillable = ['nama'];

   function products(){
     return $this->hasMany(Products::class, 'category_id', 'id');
   }
}
