<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Customers extends Model
{
   protected $table = 'customers';
   protected $fillable = ['email','first_name','last_name','address','phone_number','password'];
}
