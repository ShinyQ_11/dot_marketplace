<?php

namespace App\Http\Models;
use App\Http\Models\Categories;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Products extends Model
{
    use SoftDeletes;
    protected $table = 'products';
    protected $fillable = ['nama','category_id','unit_price','image'];
    public $timestamps = true;
    function category(){
        return $this->hasOne(Categories::class, "id", "category_id");
    }
}
