<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Session;
class ProdukController extends Controller
{
  public function index(Request $req){
      return view('produk', ['datapro'=>[] ]);
  }

  public function submit(Request $req)
	{
    // $kategori = $req->input('nama_kategori');
    $produk =  array('nama_kategori' => $req->input('nama_kategori'), 'nama_barang' => $req->input('nama_barang'), 'harga_barang' => $req->input('harga_barang'));
	  $dt_produk = json_decode($req->produk, TRUE);
		$dt_produk =  array_merge($produk, $dt_produk);
    dd($dt_produk);
  	return view('produk',['datapro' => $dt_produk]);
	}


  public function deleteSession(Request $request)
  {
      $this->session()->forget('produk');
      return redirect('/produk');
  }

  public function edit(Request $req)
  {
    $dt_produk = json_decode($req->produk,TRUE);
    $key = $req->key;
    $value = $req->nama_product;
    $dt_produk[$key] = $value;
    return view('home',['data' => $dt_produk]);
  }

  public function delete(Request $req, $key)
  {
    $dt_kategori = json_decode($req->data,true);
    unset($dt_kategori[$key]);
    return view('home',['data' => $dt_kategori]);
  }

}
