<?php

namespace App\Http\Controllers\API\V1;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;
use ApiBuilder;

class ApiController extends Controller{

  public function method_get()
  {
      $data = [
        'nama' => 'Kurniadi Ahmad Wijaya',
        'umur' => 10,
        'asal' => 'Bandung',
        'sekolah' => [
          'SMKS Telkom Malang',
          'SMP Negeri 2 Mimika',
          'SD Negeri 4 Mimika'
        ]
      ];
     return ApiBuilder::apiRespond(200, $data);
      // return $this->apiRespondBuilder(500, "Tidak Ada Data");
    }

    public function method_post(Request $request){
      try {
        $data = $request->all();
        $code = 200;

        if(!isset($request->nama))
          throw new Exception("Nama Harus Diisi", 1);
        if(!isset($request->type))
          throw new Exception("Type Harus Diisi", 1);

        if($request->type == 'siswa')
          $data['message'] = 'Halo Siswa !';
        elseif($request->type == 'guru')
          $data['message'] = 'Halo Guru !';
        else
          throw new Exception("Pilihan Tidak Diperbolehkan", 1);


        return $this->apiRespondBuilder(200, $data);
      } catch (\Exception $e) {
          $code = 500;
          $data = $e->getMessage();
      }
      return $this->apiRespondBuilder($code, $data);
    }

  }
