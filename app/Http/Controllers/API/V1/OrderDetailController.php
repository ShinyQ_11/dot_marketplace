<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Models\Orders_Detail;
use App\Http\Models\Products;
use App\Http\Models\Orders;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;
use ApiBuilder;
use Exception;

class OrderDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $counter = 1;
      $orderDetail = Orders_Detail::query();
      $pagination = 5;
      $orderDetail = $orderDetail->paginate($pagination);


      return ApiBuilder::apiRespond(200, $orderDetail);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        try {
          $this->validate($request, [
              'product_id'        => 'required',
              'quantity'        => 'required'
          ]);
           // get data Order detail by product id
           $checkProduct = Orders_detail::where('product_id', $request->product_id)->where('order_id', $id)->first();

           // mengambil data satu baris pada products dengan mengambil id pada input
           $dataProduct = Products::where('id', $request->product_id)->first();

           // check jika product sudah dimasukkan
           if ($checkProduct) {
               // menambah jumlah yang sebelumnya dengan yang baru
               $checkProduct->quantity = $checkProduct->quantity + $request->quantity;
               $checkProduct->price    = $checkProduct->price + ($request->quantity * $dataProduct->unit_price);
               $checkProduct->save();
           } else {
               // menambah poduct baru pada order
               $dataOrderDetail = new Orders_detail;
               $dataOrderDetail->order_id      = $id;
               $dataOrderDetail->product_id    = $request->product_id;
               $dataOrderDetail->quantity      = $request->quantity;
               $dataOrderDetail->price         = $request->quantity * $dataProduct->unit_price;
               $dataOrderDetail->save();
           }
           // mengambil price pada tabel order
            $getOrder = Orders_detail::where('order_id', $id)->get();
            // menjumlah semua price pada order
            $total = $getOrder->sum('price');

            // insert data total pada tabel order
            $dataOrder = Orders::find($id);
            $dataOrder->total = $total;

            $dataOrder->save();
            $code = 200;
        } catch (\Exception $e) {
              if($e instanceof ValidationException){
                $dataOrder = $e->errors();
                $code = 400;
              }else{
                $code = 500;
                $dataOrder = "An Error Has Ocurred";
              }
        }
        return ApiBuilder::apiRespond($code, $dataOrder);
      }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      try {
          $dataOrder = Orders_detail::findOrFail($id);
          $code = 200;
      } catch (\Exception $e) {
              if($e instanceof ModelNotFoundException){
                $code = 404;
                $dataOrder = "Data Not Exist";
              }else{
                $code = 500;
                $dataOrder = "An Error Has Ocurred";
              }
        }
        return ApiBuilder::apiRespond($code, $dataOrder);
      }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
