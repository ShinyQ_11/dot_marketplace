<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Models\Orders;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;
use App\Http\Models\Customers;
use App\Http\Controllers\Controller;
use ApiBuilder;
use Exception;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $counter = 1;
      $order = Orders::with("order_detail");
      $pagination = 5;
      $order = $order->paginate($pagination);

      return ApiBuilder::apiRespond(200, $order);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
          $code = 200;
          $this->validate($request, [
              'customer_id'        => 'required',
              'total'    => 'required'
          ]);
          Orders::create($request->all());
          $order =  Orders::create($request->all());
        } catch (\Exception $e) {
            if($e instanceof ValidationException){
              $order = $e->errors();
              $code = 400;
            }else{
              $code= 500;
              $order = "An Error Has Ocurred";
            }
        }

        return ApiBuilder::apiRespond($code, $order);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
          $code = 200;
          $order = Orders::with("order_detail")->findOrFail($id);
        } catch (\Exception $e) {
            if($e instanceof ModelNotFoundException){
              $code= 404;
              $order = "Data Not Exist";
            }
            else{
              $code= 500;
              $order = "An Error Has Ocurred";
            }
        }
        return ApiBuilder::apiRespond($code, $order);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
