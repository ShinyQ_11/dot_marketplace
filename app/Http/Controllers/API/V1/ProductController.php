<?php

namespace App\Http\COntrollers\API\V1;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Models\Categories;
use App\Http\Resources\ProductsResources;
use App\Http\Models\Products;

use ApiBuilder;
use Exception;
class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      try {
        $code = 200;
        $category = Categories::all();
        $product = Products::query();
        $product->latest();
        $pagination = 5;
        $product = $product->paginate($pagination);

      } catch (\Exception $e) {
        $code = 500;
        $product = "An Error Has Ocurred";
      }
       // return ApiBuilder::apiRespond($code, ProductsResources::collection($product));
      return ApiBuilder::apiRespond($code, $product);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
          $this->validate($request, [
              'nama'        => 'required',
              'category_id'    => 'required',
              'unit_price'    => 'required|numeric',
              'image'         => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
          ]);
          Categories::where('id', $request->category_id)->increment('product_count');
          $imageName = time().'.'.request()->image->getClientOriginalExtension();
          request()->image->move(public_path('images'), $imageName);
           //query store product
          $product = new Products();
          $product->nama = $request->nama;
          $product->category_id = $request->category_id;
          $product->unit_price =$request->unit_price;
          $product->image = $imageName;
          $product->save();

          $product = new ProductsResources($product);
          $code = 200;
        } catch (\Exception $e) {
            $code = 500;
            $product = "An Error Has Ocurred";
        }
          return ApiBuilder::apiRespond($code, $product);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      try {
        $product = new ProductsResources(Products::findOrFail($id));
        $code = 200;
      } catch (\Exception $e) {
        if($e instanceof ModelNotFoundException){
          $code= 404;
          $product = "Data Not Exist";
        }
        else{
          $code= 500;
          $product = "An Error Has Ocurred";
        }
      }
      return ApiBuilder::apiRespond($code, $product);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
          $this->validate($request, [
              'nama'        => 'required',
              'category_id'    => 'required',
              'unit_price'    => 'required',
              'image'    => 'required', 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
          ]);
          $imageName = time().'.'.request()->image->getClientOriginalExtension();
          request()->image->move(public_path('images'), $imageName);

          $product = Products::findOrFail($id);
          $product->nama = $request->nama;
          $product->category_id = $request->category_id;
          $product->unit_price = $request->unit_price;
          $product->image = $imageName;
                    // dd($product);
          $product->save();
          $code = 200;
        } catch (\Exception $e) {
          if($e instanceof ValidationException){
            $product = $e->errors();
            $code = 400;
          }
          elseif($e instanceof ModelNotFoundException){
            $code= 404;
            $product = "Data Not Exist";
          }
          else{
            $code= 500;
            $product = $e;
          }
        }
        return ApiBuilder::apiRespond($code, $product);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
