<?php

namespace App\Http\COntrollers\API\V1;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Models\Customers;
use ApiBuilder;
use Exception;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      try {
        $code = 200;
        $counter = 1;
        $customer = Customers::query()->latest();
        $pagination = 5;
        $customer = $customer->paginate($pagination);
        
      } catch (\Exception $e) {
        $code = 500;
        $customer = "An Error Has Ocurred";
      }
      return ApiBuilder::apiRespond($code, $customer);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
          $this->validate($request, [
              'email'        => 'required|min:5',
              'first_name'    => 'required',
              'last_name'    => 'required',
              'address'    => 'required',
              'phone_number'    => 'required',
              'password'    => 'required',
          ]);
          Customers::create($request->except("_token"));
        } catch (\Exception $e) {
          if($e instanceof ValidationException){
            $customer = $e->errors();
            $code = 400;
          }elseif($e instanceof ModelNotFoundException){
            $code= 404;
            $category = "Data Not Exist";
          }
          else{
            $code= 500;
            $category = "An Error Has Ocurred";
          }

          return ApiBuilder::apiRespond($code, $customer);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
          $customer = Customers::findOrFail($id);
          $code = 200;
        } catch (\Exception $e) {
          if($e instanceof ModelNotFoundException){
            $code= 404;
            $customer = "Data Not Exist";
          }
          else{
            $code= 500;
            $customer = "An Error Has Ocurred";
          }
        }
        return ApiBuilder::apiRespond($code, $customer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
          $code = 200;
          $this->validate($request, [
              'email'        => 'required|min:5',
              'first_name'    => 'required',
              'last_name'    => 'required',
              'address'    => 'required',
              'phone_number'    => 'required',
              'password'    => 'required',
          ]);

          $customer = Customers::findOrFail($id);
          $customer->email = $request->email;
          $customer->first_name = $request->first_name;
          $customer->last_name = $request->last_name;
          $customer->address = $request->address;
          $customer->phone_number = $request->phone_number;
          $customer->password = $request->password;
          $customer->save();
        } catch (\Exception $e) {
          if($e instanceof ValidationException){
            $customer = $e->errors();
            $code = 400;
          }
          elseif($e instanceof ModelNotFoundException){
            $code= 404;
            $customer = "Data Not Exist";
          }
          else{
            $code= 500;
            $customer = "An Error Has Ocurred";
          }
        }
        return ApiBuilder::apiRespond($code, $customer);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
          $customer = Customers::findOrFail('id', $id);
          $customer = $customer->delete();
        } catch (\Exception $e) {
          if($e instanceof ModelNotFoundException){
            $code= 404;
            $customer = "Data Not Exist";
          }
          else{
            $code= 500;
            $customer = "An Error Has Ocurred";
          }
        }
          return ApiBuilder::apiRespond($code, $customer);

    }
}
