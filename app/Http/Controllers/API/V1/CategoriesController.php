<?php

namespace App\Http\COntrollers\API\V1;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Models\Categories;
use ApiBuilder;
use Exception;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      try {
        $code = 200;
        $counter = 1;
        $category = Categories::query()->latest();
        $pagination = 5;
        $category = $category->paginate($pagination);
      } catch (\Exception $e) {
        $code = 500;
        $category = "An Error Has Ocurred";
      }
      return ApiBuilder::apiRespond($code, $category);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      try {
        $code= "200";
        $category = new Categories();
        $category->nama = $request->input('nama');
        $category->product_count = $request->input('product_count');
        $category->save();
      } catch (\Exception $e) {
        $code= "500";
        $category = "An Error Has Ocurred";
      }

      return ApiBuilder::apiRespond($code, $category);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
      try {
        $code= "200";
        $category = Categories::with("products")->findOrFail($id);
      }
      catch (\Exception $e) {
        if($e instanceof ValidationException){
          $category = $e->errors();
          $code = 400;
        }
        elseif($e instanceof ModelNotFoundException){
          $code= 404;
          $category = "Data Not Exist";
        }
        else{
          $code= 500;
          $category = "An Error Has Ocurred";
        }
      }
      return ApiBuilder::apiRespond($code, $category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

      try {
        $this->validate($request, [
            'nama'        => 'required',
            'product_count'        => 'required'
        ]);
        $code = 200;
        $category = Categories::findOrFail($id);
        $category->nama = $request->nama;
        $category->product_count = $request->product_count;
        $category->save();
      } catch (\Exception $e) {
        if($e instanceof ValidationException){
          $category = $e->errors();
          $code = 400;
        }
        elseif($e instanceof ModelNotFoundException){
          $code= 404;
          $category = "Data ID Not Exist";
        }
        else{
          $code= 500;
          $category = $e;
        }
      }
      return ApiBuilder::apiRespond($code, $category);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      try {
        Categories::findOrFail('id',$id)->delete();
        $category = "Sukses Delete Data";
        $code = 200;
      } catch (\Exception $e) {
        $code= 500;
        $category = "An Error Has Ocurred";
      }
      return ApiBuilder::apiRespond($code, $category);
    }
}
