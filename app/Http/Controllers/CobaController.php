<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CobaController extends Controller
{
    public function index(){
      return "<h1>INI HALAMAN INDEX</h1>";
    }

    public function sapa(){
      return "<h1>Halo Pengguna</h1>";
    }

    public function sapaLengkap($nama, $sapa){
      return "<h1>Halo $nama, Selamat $sapa.</h1>";
    }

}
