<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use ApiBuilder;
use App\User;
use Bcrypt;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
  public function index(){
    return view('Auth/register');
  }

  public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        if ($validator->fails()) {
            return ApiBuilder::apiResponseValidationFails('Validation error messages!', $validator->errors()->all());
        }

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Bcrypt($request->password)
        ]);

        $success['user'] = $user;
        $success['token'] = $user->createToken('myApp')->accessToken;

        return ApiBuilder::apiResponseSuccess('Register Sukses!', $success, 200);
    }
}
