<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class KategoriController extends Controller
{
  public function index(){
    return view('home', ['data' => []]);

  }

  public function submit(Request $req)
	{
		$kategori = $req->input('nama_kategori');
		$dt_kategori = json_decode($req->kategori,TRUE);
		$dt_kategori = array_merge($kategori,$dt_kategori);
    return view('home',['data' => $dt_kategori]);
	}

  public function edit(Request $req)
	{
		$dt_kategori = json_decode($req->kategori,TRUE);
		$key = $req->key;
		$value = $req->nama_kategori;
		$dt_kategori[$key] = $value;
		return view('home',['data' => $dt_kategori]);
	}

	public function delete(Request $req, $key)
	{
		$dt_kategori = json_decode($req->data,true);
		unset($dt_kategori[$key]);
		return view('home',['data' => $dt_kategori]);
	}

}
