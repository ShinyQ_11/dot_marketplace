<?php

namespace App\Http\Controllers\Marketplace;

use Illuminate\Http\Request;
use App\Http\Models\Customers;
use App\Http\Controllers\Controller;
use Session;
use ApiBuilder;

class ControllerCustomer extends Controller
{
    // public function __construct()
    // {
    //   $this->middleware(function ($request, $next){
    //     if(!Session::get('login'))
    //     return redirect('login');
    //
    //     return $next($request);
    //   });
    // }

    public function getApi()
    {
      $counter = 1;
      $customer = Customers::query();
    //   return view('marketplace/customer',['dataCustomer'=> $customer,'counter'=>$counter]);
      if (request()->has("search") && strlen(request()->query("search")) >= 1) {
        $customer->where(
          "customers.first_name", "like", "%" . request()->query("search") . "%"
        )->orWhere("customers.last_name", "like", "%" . request()->query("search") . "%");
      }

      $pagination = 5;
      $customer = $customer->paginate($pagination);
      if( request()->has('page') && request()->get('page') > 1){
        $counter += (request()->get('page')- 1) * $pagination;
      }

      return ApiBuilder::apiRespond(200, $customer);
    }

    public function detailApi($id)
    {
      $customer = Customers::find($id);
      return ApiBuilder::apiRespond(200, $customer);
    }

    public function addApi(Request $request)
    {
      $this->validate($request, [
          'email'        => 'required|min:5',
          'first_name'    => 'required',
          'last_name'    => 'required',
          'address'    => 'required',
          'phone_number'    => 'required',
          'password'    => 'required',
      ]);
      Customers::create($request->except("_token"));
      Session::flash('sukses', 'Berhasil Menambahkan Data Costumer');
      return ApiBuilder::apiRespond(200,   Customers::create($request->except("_token")));
    }

    public function updateApi($id, Request $request)
    {
      $this->validate($request, [
          'email'        => 'required|min:5',
          'first_name'    => 'required',
          'last_name'    => 'required',
          'address'    => 'required',
          'phone_number'    => 'required',
          'password'    => 'required',
      ]);

      $customer = Customers::find($id);
      $customer->email = $request->email;
      $customer->first_name = $request->first_name;
      $customer->last_name = $request->last_name;
      $customer->address = $request->address;
      $customer->phone_number = $request->phone_number;
      $customer->password = $request->password;
      $customer->save();
      return ApiBuilder::apiRespond(200, $customer);
    }

    public function index()
    {
      $counter = 1;
      $customer = Customers::query();
    //   return view('marketplace/customer',['dataCustomer'=> $customer,'counter'=>$counter]);
      if (request()->has("search") && strlen(request()->query("search")) >= 1) {
        $customer->where(
          "customers.first_name", "like", "%" . request()->query("search") . "%"
        )->orWhere("customers.last_name", "like", "%" . request()->query("search") . "%");
      }

      $pagination = 5;
      $customer = $customer->paginate($pagination);
      if( request()->has('page') && request()->get('page') > 1){
        $counter += (request()->get('page')- 1) * $pagination;
      }

      return view(
        'marketplace/customer',
        compact('customer','counter')
      );
    }

    public function tambahCustomer(Request $request)
    {
        $this->validate($request, [
            'email'        => 'required|min:5',
            'first_name'    => 'required',
            'last_name'    => 'required',
            'address'    => 'required',
            'phone_number'    => 'required',
            'password'    => 'required',
        ]);
        Customers::create($request->except("_token"));
        Session::flash('sukses', 'Berhasil Menambahkan Data Costumer');
        return redirect()->back();
    }

    public function detail($id)
    {
      $customer = Customers::find($id);
      return view(
        'marketplace/customer_edit',
        compact('customer')
      );
    }

    public function edit_customer($id)
    {
      $customer = Customers::find($id);
      return view(
        'marketplace/customer_edit',
        compact('customer')
      );
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'email'        => 'required|min:5',
            'first_name'    => 'required',
            'last_name'    => 'required',
            'address'    => 'required',
            'phone_number'    => 'required',
            'password'    => 'required',
        ]);

        $customer = Customers::find($id);
        $customer->email = $request->email;
        $customer->first_name = $request->first_name;
        $customer->last_name = $request->last_name;
        $customer->address = $request->address;
        $customer->phone_number = $request->phone_number;
        $customer->password = $request->password;
        $customer->save();

        if($customer){
          Session::flash('sukses', 'Berhasil Mengupdate Data Costumer');
        }else{
          Session::flash('gagal', 'Gagal Mengupdate Data Costumer');
        }
        return redirect()->back();
    }

    public function hapus($id)
    {
      Customers::where('id', $id)->delete();
      Session::flash('sukses', 'Berhasil Menghapus Data Costumer');
      return redirect('customer');
    }

    public function logout(){
      Session::flush();
      return redirect('/login');
    }
}
