<?php

namespace App\Http\Controllers\Marketplace;

use Illuminate\Http\Request;
use App\Http\Models\Categories;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Session;
use Exception;
use SoftDeletes;
use ApiBuilder;
use Illuminate\Support\Facades\Auth;

class ControllerCategory extends Controller
{
  // public function __construct()
  // {
  //   $this->middleware(function ($request, $next){
  //     if(!Session::get('login'))
  //     return redirect('login');
  //
  //     return $next($request);
  //   });
  // }

  public function getApi()
  {
    // $id = Auth::id();
    $counter = 1;
    $category = Categories::query();
    $category->latest();
    if (request()->has("search") && strlen(request()->query("search")) >= 1) {
      $category->where(
        "categories.nama", "like", "%" . request()->query("search") . "%"
      );
    }
    $pagination = 5;
    $category = $category->paginate($pagination);
    if( request()->has('page') && request()->get('page') > 1){
      $counter += (request()->get('page')- 1) * $pagination;
    }

    return ApiBuilder::apiRespond(200, $category);
  }

  public function detailApi($id)
  {
    $category = Categories::with("products")->find($id);
    return ApiBuilder::apiRespond(200, $category);
  }

  public function addApi(Request $request)
  {
      $category = new Categories();
      $category->nama = $request->input('nama');
      $category->product_count = $request->input('product_count');
      $category->save();
      return ApiBuilder::apiRespond(200, $category);
  }

  public function updateApi(Request $request, $id){

        $this->validate($request, [
            'nama'        => 'required',
        ]);

        $category = Categories::find($id);
        $category->nama = $request->nama;
        $category->product_count = $request->product_count;
        $category->save();
        return ApiBuilder::apiRespond(200, $category);
    }

  public function index()
  {
    $counter = 1;
    $category = Categories::query();
    $category->latest();
    if (request()->has("search") && strlen(request()->query("search")) >= 1) {
      $category->where(
        "categories.nama", "like", "%" . request()->query("search") . "%"
      );
    }

    $pagination = 5;
    $category = $category->paginate($pagination);
    if( request()->has('page') && request()->get('page') > 1){
      $counter += (request()->get('page')- 1) * $pagination;
    }

    return view(
      'marketplace/kategori',
       compact('category','counter'),
     );
  }

  public function detail($id)
  {
    $category = Categories::with("products")->find($id);
    return view(
      'marketplace/kategori_detail',
      compact('category')
    );
  }

  public function tambahcategory(Request $request)
  {
      try {
        \DB::beginTransaction();
        $category = new Categories($request->except("_token"));
        $category->save();
        return ApiBuilder::apiRespond(200, $category);
        \DB::commit();
        Session::flash('sukses', 'Sukses Menambahkan Category');
      } catch (Exception $e) {
        \DB::rollBack();
            Session::flash('gagal', 'Data gagal ditambahkan');
      }
      return redirect()->back();
  }

  public function edit_kategori($id)
  {
    $category = Categories::find($id);
    return view(
      'marketplace/kategori_edit',
      compact('category')
    );
  }

  public function update(Request $request, $id){

        $this->validate($request, [
            'nama'        => 'required',
        ]);

        $category = Categories::find($id);
        $category->nama = $request->nama;
        $category->save();
        Session::flash('sukses', 'Sukses Mengupdate Category');
        return redirect()->back();
    }

  public function hapus($id)
  {
    try {
      \DB::beginTransaction();
      Categories::where('id',$id)->delete();
      \DB::commit();

    } catch (Exception $e) {
      \DB::rollBack();
      Session::flash('sukses', 'Data gagal dihapus');
    }
    return redirect()->back();
  }



}
