<?php

namespace App\Http\Controllers\Marketplace;

use Illuminate\Http\Request;
use App\Http\Models\Products;
use App\Http\Models\Categories;
use App\Http\Controllers\Controller;
use Session;
use Validator;
use Exception;
use SoftDeletes;


class ControllerProduct extends Controller
{

  // public function __construct()
  // {
  //   $this->middleware(function ($request, $next){
  //     if(!Session::get('login'))
  //     return redirect('login');
  //
  //     return $next($request);
  //   });
  // }

  public function index()
  {
    $category = Categories::all();
    $product = Products::query();
    $product->latest();
    // Search data
		// if search query exist and length more than one
		if (request()->has("search") && strlen(request()->query("search")) >= 1) {
			$product->where(
				"products.nama", "like", "%" . request()->query("search") . "%"
			);
		}

    // Query Pagination
    $pagination = 5;
    $product = $product->paginate($pagination);

    // Handle Page pagination
    $counter = 1;
    if( request()->has('page') && request()->get('page') > 1){
      $counter += (request()->get('page')- 1) * $pagination;
    }

    return view('marketplace/produk', compact('product','counter','category'));
  }

  public function detail($id)
  {
    $product = Products::find($id);
    return view('marketplace/produk_detail', compact('product'));
  }

  public function tambahProduct(Request $request)
  {

    $this->validate($request, [
        'nama'        => 'required|min:2',
        'category_id'    => 'required',
        'unit_price'    => 'required|min:2|numeric',
        'image'         => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
    ]);

    try {
      \DB::beginTransaction();
      Categories::where('id', $request->category_id)->increment('product_count');
      $imageName = time().'.'.request()->image->getClientOriginalExtension();
      request()->image->move(public_path('images'), $imageName);
       //query store product
      $product = new Products();
      $product->nama = $request->nama;
      $product->category_id = $request->category_id;
      $product->unit_price =$request->unit_price;
      $product->image = $imageName;
      $product->save();

      // $dataProducts = Products::latest();
      // $dataCategory = Categories::where('id', $dataProducts->category_id)->first();
      // $dataCategory->product_count = $dataCategory->product_count + 1;
      // $dataCategory->save();

      \DB::commit();

       //update product count
      // Categories::find($request->categori_id)->increment('product_count');
      Session::flash('sukses', 'Berhasil menambahkan data');
      return redirect()->back();

    } catch (Exception $e) {
      // dd($e);
      \DB::rollBack();
          Session::flash('gagal', 'Data gagal ditambahkan');
    }
    return redirect()->back();
  }

  public function edit_product($id)
  {
    $product = Products::find($id);
    $category = Categories::all();
    return view(
      'marketplace/produk_edit',
      compact('product','category')
    );
  }

  public function update(Request $request, $id){

        $this->validate($request, [
            'nama'        => 'required',
            'category_id'    => 'required',
            'unit_price'    => 'required',
            'image'    => 'required', 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
        $imageName = time().'.'.request()->image->getClientOriginalExtension();
        request()->image->move(public_path('images'), $imageName);

        $product = Products::find($id);
        $product->nama = $request->nama;
        $product->category_id = $request->category_id;
        $product->unit_price = $request->unit_price;
        $product->image = $imageName;
        $product->save();

        Session::flash('sukses', 'Berhasil Mengupdate Data Product');
        return redirect()->back();
    }

    public function hapus($id)
    {
      $product = Products::find($id);
      $product->delete();

      $dataCategory = Categories::where('id', $product->category_id)->first();
      $dataCategory->product_count = $dataCategory->product_count - 1;
      $dataCategory->save();

      Session::flash('sukses', 'Berhasil Menghapus Data Product');
      return redirect()->back();
    }


}
