<?php

namespace App\Http\Controllers\Marketplace;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Models\Categories;
use App\Http\Models\Customers;
use App\Http\Models\Products;
use App\Http\Models\Orders;
use App\Http\Models\Orders_detail;
use Session;

class ControllerOrder extends Controller
{

    // public function __construct()
    // {
    //   $this->middleware(function ($request, $next){
    //     if(!Session::get('login'))
    //     return redirect('login');
    //
    //     return $next($request);
    //   });
    // }

    public function index(){
      $category = Categories::all();
      $product = Products::all();
      $order = Orders::all();
      $orderDetail = Orders_detail::all();
      $customer = Customers::all();
      $counter = 1;
      return view(
        'marketplace/order',
        compact('product',
                'order',
                'customer',
                'category',
                'orderDetail',
                'counter')
        );
    }

    public function tambahOrder(Request $request){
      Orders::create($request->all());
        Session::flash('sukses', 'Sukses Menambahkan Pengguna');
        return redirect()->back();
    }

    public function detailOrder($id){
      $dataOrderDetail = Orders_detail::where('order_id', $id)->get();
      $detailId = $id;
      $total = Orders::select('total')->where('id', $id)->first();


      $category = Categories::all();
      $product = Products::all();
      $order = Orders::all();
      $orderDetail = Orders_detail::all();
      $customer = Customers::all();
      $counter = 1;
      return view(
        'marketplace/order_detail',
        compact('product',
                'order',
                'total',
                'customer',
                'category',
                'detailId',
                'orderDetail',
                'dataOrderDetail',
                'counter')
        );
    }


    public function tambahProduct(Request $request, $id)
    {
      // get data Order detail by product id
      $checkProduct = Orders_detail::where('product_id', $request->product_id)->where('order_id', $id)->first();
           // mengambil data satu baris pada products dengan mengambil id pada input
           $dataProduct = Products::where('id', $request->product_id)->first();

           // check jika product sudah dimasukkan
           if ($checkProduct) {
               // menambah jumlah yang sebelumnya dengan yang baru
               $checkProduct->quantity = $checkProduct->quantity + $request->quantity;
               $checkProduct->price    = $checkProduct->price + ($request->quantity * $dataProduct->unit_price);
               $checkProduct->save();
           } else {
               // menambah poduct baru pada order
               $dataOrderDetail = new Orders_detail;
               $dataOrderDetail->order_id      = $id;
               $dataOrderDetail->product_id    = $request->product_id;
               $dataOrderDetail->quantity      = $request->quantity;
               $dataOrderDetail->price         = $request->quantity * $dataProduct->unit_price;
               $dataOrderDetail->save();
           }

           // mengambil price pada tabel order
            $getOrder = Orders_detail::where('order_id', $id)->get();

            // menjumlah semua price pada order
            $total = $getOrder->sum('price');

            // insert data total pada tabel order
            $dataOrder = Orders::find($id);
            $dataOrder->total = $total;
            $dataOrder->save();

            return redirect()->back();
    }

    public function hapusOrder($id)
    {
       $dataOrder = Orders::find($id);
       $dataOrder->delete();

       return redirect()->back();

    }

}
