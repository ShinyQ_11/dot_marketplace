@if (Session::has('sukses') || Session::has('gagal'))
  <script>
    $(document).ready(function(){
        $("#pesan").modal();
    });
  </script>
@endif
<!-- Modal Pesan -->
<div class="modal fade" id="pesan" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">
              <center>
                @if (Session::has('sukses'))
                   <font color="green" size="4px"><b>{{ Session::get('sukses') }}</b></font>
                @else
                   <font color="red" size="4px"><b>{{ Session::get('gagal') }}</b></font>
                @endif

              </center>
            </h4>
        </div>
    </div>
  </div>
</div>
<footer class="main-footer">
  <div class="pull-right hidden-xs">
    <b>Version</b> 2.4.0
  </div>
  <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
  reserved.
</footer>

</div>
<!-- ./wrapper -->

<!-- jQuery Knob Chart -->
{{-- <script src="{{url('assets/bower_components/jquery-knob/dist/jquery.knob.min.js')}}"></script> --}}
<!-- daterangepicker -->
{{-- <script src="{{url('assets/bower_components/moment/min/moment.min.js')}}"></script>
<script src="{{url('assets/bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script> --}}
<!-- datepicker -->
<script src="{{url('assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{url('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
<!-- Slimscroll -->
<script src="{{url('assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>

<!-- AdminLTE App -->
<script src="{{url('assets/dist/js/adminlte.min.js')}}"></script>

</body>
</html>
