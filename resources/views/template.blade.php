@include('base/header')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->


    <!-- Main content -->
    <section class="content">
        @yield('konten')
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@include('base/footer')

<script type="text/javascript">
$('.btn-edit').on('click', function() {
    $('#modal-default').modal('show');
    $('#nama_kategori').val($(this).data('kategori'));
    $('#key').val($(this).data('id'));
});

</script>
