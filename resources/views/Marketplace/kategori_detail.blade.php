@extends('template')

@section('konten')

      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"><strong>Tabel Data Kategori {{ $category->nama }}</strong></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered">
                <tr>
                  <th>Id</th>
                  <th>First Name</th>
                </tr>
                @forelse ($category->products as $data )
                  <tr>
                    <td>{{ $data->id  }}</td>
                    <td>{{ $data->nama }}</td>
                  </tr>
                @empty
                    <td>Belum Ada Data Pada Kategori Ini</td>
                    <td></td>

                @endforelse

              </table><br />
              <a class="btn btn-primary" href="/kategori">Kembali Ke Kategori</a>
            </div>
          </div>
      </div>

@endsection
