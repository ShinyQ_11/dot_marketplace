@extends('template')

@section('konten')
  <div class="row">
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h4><strong>Tambah Data Customer</strong></h4>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="/customer/tambah" method="post">

              <div class="row">
                <div class="col-md-6">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Email</label>
                      <input type="text" name="email" class="form-control" placeholder="Email">
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Nomor Telepon</label>
                      <input type="text" name="phone_number" class="form-control" placeholder="Nomor Telepon">
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-6">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Nama Depan</label>
                      <input type="text" name="first_name" class="form-control" placeholder="Nama Depan">
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Nama Belakang</label>
                      <input type="text" name="last_name" class="form-control" placeholder="Nama Belakang">
                    </div>
                  </div>
                </div>
              </div>


              <div class="row">
                <div class="col-md-6">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Alamat</label>
                      <input type="text" name="address" class="form-control" placeholder="Alamat">
                    </div>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Password</label>
                      <input type="password" name="password" class="form-control" placeholder="Password">
                    </div>
                  </div>
                </div>
              </div>

              {{ csrf_field() }}
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">+ Tambah Data</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><strong>Tabel Data Customer</strong></h3>
              <form action="/customer" method="GET">
                <span class="pull-right">
                  <input type="text" name="search" class="form-control" placeholder="Search here ..">
                </span>
              </form>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered">
                <tr>
                  <th>No</th>
                  <th>Email</th>
                  <th>First Name</th>
                  <th>Last Name</th>
                  <th>Address</th>
                  <th>Phone Number</th>
                  <th>Action</th>
                </tr>
                @foreach ($customer as $data )
                  <tr>
                    <td>{{ $counter++ }}</td>
                    <td>{{ $data->email }}</td>
                    <td>{{ $data->first_name }}</td>
                    <td>{{ $data->last_name }}</td>
                    <td>{{ $data->address }}</td>
                    <td>{{ $data->phone_number }}</td>
                    <td>
                      <a class="btn btn-warning" href="/customer/edit/{{ $data->id }}"><i class="fa fa-edit"></i> Edit Data</a>
                      <a class="btn btn-danger" href="/customer/hapus/{{ $data->id }}"><i class="fa fa-trash"></i> Hapus</a>
                    </td>
                  </tr>
                @endforeach
              </table>
            </div>

            <div class="text-center">
                {!! $customer->appends(request()->all())->links() !!}
            </div>

          </div>
        </div>
      </div>

@endsection
