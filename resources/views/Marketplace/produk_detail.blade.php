@extends('template')

@section('konten')

      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Detail Produk</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered">
                <tr>
                  <td>Nama Barang</td>
                  <td>:</td>
                  <td>{{ $product->nama }}</td>
                </tr>

                <tr>
                  <td>Nama Kategori</td>
                  <td>:</td>
                  <td>{{ $product->category->nama }}</td>
                </tr>

                <tr>
                  <td>Harga Barang</td>
                  <td>:</td>
                  <td>Rp{{ number_format($product->unit_price,2,',','.') }}</td>
                </tr>

                <tr>
                  <td>Harga Barang</td>
                  <td>:</td>
                  <td>
                    @if ($product->image)
                      <img src="{{asset('images')}}/{{ $product->image }}" width="200px"/>
                    @else
                    Tidak Ada Gambar
                    @endif
                  </td>
                </tr>

              </table>
                <br />
                <a href="/produk" type="submit" class="btn btn-primary">Kembali </a> &nbsp;&nbsp;
                <a href="/produk/edit/{{ $product->id }}" class="btn btn-warning">Edit Data</a>
            </div>
          </div>
      </div>
@endsection
