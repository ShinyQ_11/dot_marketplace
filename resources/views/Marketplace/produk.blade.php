@extends('template')

@section('konten')
  <div class="row">
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h4><strong>Tambah Data Barang</strong></h4>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="/produk/tambah" method="post" enctype="multipart/form-data">
              <div class="col-md-6">

              @if($errors->has('nama'))
                <div style="padding-left:10px">
                  <h5><strong><font color="red">{{ $errors->first('nama')}}</font></strong></h5>
                </div>
              @endif
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Nama Barang</label>
                  <input type="text" name="nama" class="form-control" placeholder="Nama Barang">
                </div>
              </div>
              </div>

              <div class="col-md-6">
                @if($errors->has('category_id'))
                  <div style="padding-left:10px">
                    <h5><strong><font color="red">{{ $errors->first('category')}}</font></strong></h5>
                  </div>
                @endif
                <div class="box-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Kategori Barang</label>
                    <select type="text" class="form-control" name="category_id">
                        <option></option>
                      @foreach ($category as $data)
                        <option value="{{ $data->id }}">{{ $data->nama }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
              </div>

              <div class="col-md-6">
              @if($errors->has('unit_price'))
                <div style="padding-left:10px">
                  <h5><strong><font color="red">{{ $errors->first('unit_price')}}</font></strong></h5>
                </div>
              @endif

              <div class="box-body">
                @if($errors->has('harga'))
                  <div style="padding-left:10px">
                    <h5><strong><font color="red">{{ $errors->first('harga')}}</font></strong></h5>
                  </div>
                @endif
                <div class="form-group">
                  <label for="exampleInputEmail1">Harga Barang</label>
                  <input type="number" name="unit_price" class="form-control" placeholder="Harga Barang">
                </div>
              </div>
            </div>

                <div class="col-md-6">
                  @if($errors->has('image'))
                    <div style="padding-left:10px">
                      <h5><strong><font color="red">{{ $errors->first('image')}}</font></strong></h5>
                    </div>
                  @endif
                <div class="box-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Foto Barang</label>
                    <input type="file" name="image" class="form-control" placeholder="Harga Barang">
                  </div>
                </div>
              </div>

                @csrf
                <div class="box-footer" style="margin-left:16px;">
                  <button type="submit" class="btn btn-primary">Tambah Data Product</button>
                </div><br />
            </form>
          </div>
          <!-- /.box -->
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h4><strong>Tabel Data Barang</strong></h4>
              <form action="/produk" method="GET">
                <span class="pull-right">
                  <input type="text" name="search" class="form-control" placeholder="Search here ..">
                </span>
              </form>

            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered">
                <tr>
                  <th>No</th>
                  <th>Nama Barang</th>
                  <th>Nama Kategori</th>
                  <th>Harga Barang</th>
                  <th>Foto</th>
                  <th>Action</th>
                </tr>
                @foreach ($product as $data)
                  <tr>
                    <td>{{ $counter++ }}</td>
                    <td><a href="/produk/{{ $data->id }}">{{ $data->nama }}</a></td>
                    <td>{{ $data->category->nama }}</td>
                    <td>Rp{{ number_format($data->unit_price,2,',','.') }}</td>
                    <td>
                      @if ($data->image)
                        <img src="{{asset('images')}}/{{ $data->image }}" width="200px"/>
                      @else
                      Tidak Ada Gambar
                      @endif
                    </td>
                    <td>
                      <a class="btn btn-primary" href="/produk/{{ $data->id }}">Detail Produk</a>
                      <a class="btn btn-warning" href="/produk/edit/{{ $data->id }}">Edit Produk</a>
                      <a class="btn btn-danger" href='/produk/hapus/{{ $data->id }}'>Hapus Produk</a>
                    </td>
                  </tr>
                @endforeach
              </table>
            </div>
            <div class="text-center">
                {!! $product->appends(request()->all())->links() !!}
            </div>

          </div>
      </div>

      </script>
@endsection
