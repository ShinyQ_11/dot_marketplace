@extends('template')

@section('konten')

      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Detail Produk</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form role="form" action="/produk/{{$product->id}}" method="post" enctype="multipart/form-data">
                <table class="table table-bordered">
                  <tr>
                    <td>Nama Barang</td>
                    <td>:</td>
                    <td><input type="text" name="nama" value="{{ $product->nama }}" class="form-control" ></td>
                  </tr>

                  <tr>
                    <td>Nama Kategori</td>
                    <td>:</td>
                    <td>
                      <select type="text" class="form-control"  id="kategori_id" name="category_id" >

                        @foreach ($category as $data)
                          <option value="{{ $data->id }}">{{ $data->nama }}</option>
                        @endforeach
                      </select>
                    </td>
                  </tr>

                  <tr>
                    <td>Harga Barang</td>
                    <td>:</td>
                    <td><input type="text" name="unit_price" value="{{ $product->unit_price }}" class="form-control" ></td>
                  </tr>

                  <tr>
                    <td>Foto Barang</td>
                    <td>:</td>
                    <td><input type="file" name="image" class="form-control" placeholder="Tambah Foto"></td>
                  </tr>

                  {{ csrf_field() }}
                  <input type="hidden" name="_method" value="PUT">

                </table>

                  <a href="/produk/{{ $product->id }}" type="submit" class="btn btn-primary">Kembali</a> &nbsp;&nbsp;
                  <button type="submit" class="btn btn-warning">Edit Data Produk</button>
              </form>
            </div>
          </div>
      </div>

      <script type="text/javascript">
          $('#kategori_id').val("{{$product->category_id}}");
      </script>
@endsection
