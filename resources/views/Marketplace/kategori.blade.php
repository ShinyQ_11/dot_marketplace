@extends('template')

@section('konten')
  <div class="row">
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h4><strong>Tambah Data Barang</strong></h4>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="/kategori" method="post">

              @if($errors->has('category'))
                <div style="padding-left:10px">
                  <h5><strong><font color="red">{{ $errors->first('category')}}</font></strong></h5>
                </div>
              @endif
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Nama Kategori</label>
                  <input type="text" name="nama" class="form-control" placeholder="Nama Kategori">
                </div>
              </div>
              <!-- /.box-body -->
                <input type="hidden" name="product_count" class="form-control" value="0">
              {{ csrf_field() }}
              <div class="box-footer">
                <button type="submit" class="btn btn-primary pull-right ">Tambah Kategori</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>

        <div class="col-md-6">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h4><strong>Tabel Data Kategori</strong></h4>
              <form action="/kategori" method="GET">
                <span class="pull-right">
                  <input type="text" name="search" class="form-control" placeholder="Search here ..">
                </span>
              </form>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered">
                <tr>
                  <th>No</th>
                  <th>Nama Kategori</th>
                  <th>Product Count</th>
                  <th>Action</th>
                </tr>
                @foreach ($category as $data)
                  <tr>
                    <td>{{ $counter++ }}</td>
                    <td>{{ $data->nama }}</td>
                    <td>{{ $data->product_count }}</td>
                    <td>
                      <a class="btn btn-primary" href="/kategori/detail/{{ $data->id }}"><i class="fa fa-eye"></i></a>
                      <a class="btn btn-warning" href="/kategori/edit/{{ $data->id }}"><i class="fa fa-edit"></i></a>
                      <a class="btn btn-danger" href='/kategori/hapus/{{ $data->id }}'><i class="fa fa-trash"></i></a>
                    </td>
                  </tr>
                @endforeach
              </table>
            </div>
            <div class="text-center">
                {!! $category->appends(request()->all())->links() !!}
            </div>

          </div>
      </div>
@endsection
