@extends('template')

@section('konten')
  <div class="row">
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h4><strong>Tambah Data Barang</strong></h4>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="/order" method="post" enctype="multipart/form-data">

              @if($errors->has('nama'))
                <div style="padding-left:10px">
                  <h5><strong><font color="red">{{ $errors->first('nama')}}</font></strong></h5>
                </div>
              @endif
              <div class="box-body">
                <div class="form-group">
                    <label for="exampleInputEmail1">Nama Customer :</label>
                  <select type="text" class="form-control" name="customer_id">
                      <option></option>
                    @foreach ($customer as $data)
                      <option value="{{ $data->id }}">{{ $data->first_name }} {{ $data->last_name }}</option>
                    @endforeach
                  </select>
                </div>
              </div>

              <input type="hidden" name="total" value="0" />

                @csrf
                <div class="box-footer" style="margin-left:16px;">
                  <button type="submit" class="btn btn-primary">Tambah Data Product</button>
                </div><br />
            </form>
          </div>
          <!-- /.box -->
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"><strong>Tabel Data Customer</strong></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered">
                <tr>
                  <th>No</th>
                  <th>First Name</th>
                  <th>Total</th>
                  <th>Action</th>
                </tr>
                @foreach ($order as $data )
                  <tr>
                    <td>{{ $counter++ }}</td>
                    <td>{{ $data->customer->first_name }} {{ $data->customer->last_name }}</td>
                    <td>Rp{{ number_format($data->total,2,',','.') }}</td>
                    <td>
                       <a class="btn btn-primary" href="/order/detail/{{ $data->id }}">Detail Order</a>
                       <a class="btn btn-danger" href="/order/hapus/{{ $data->id }}">Hapus Data</a>
                    </td>
                  </tr>
                @endforeach

              </table>
            </div>
          </div>
      </div>

      <div class="modal fade" id="modal-default">
          <div class="modal-dialog">
            <div class="modal-content">
              <form method="post" action="kategori/edit">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Edit Costumer</h4>
              </div>
              <div class="modal-body">
                <div class="form-group">
                  <label>Email</label>
                  <input type="text" name="email" class="form-control" id="email">
                  <br />

                  <div class="row">
                    <div class="col-md-6">
                      <label>First Name</label>
                      <input type="text" name="first_name" class="form-control" id="f_name">
                    </div>
                    <div class="col-md-6">
                      <label>Last Name</label>
                      <input type="text" name="last_name" class="form-control" value="{{ json_encode($data->last_name,true) }}" id="l_name">
                    </div>
                  </div>


                  <input type="text" name="key" id="key">
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Ubah Data</button>
              </div>
              </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
      </div>
@endsection
