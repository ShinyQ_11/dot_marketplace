@extends('template')

@section('konten')

      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Data Customer</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form role="form" action="/customer/{{$customer->id}}" method="post">
                <table class="table table-bordered">
                  <tr>
                    <td>ID Customer</td>
                    <td>:</td>
                    <td><input readonly="readonly" type="text" name="first_name" value="{{ $customer->id }}" class="form-control" ></td>
                  </tr>

                  <tr>
                    <td>Email</td>
                    <td>:</td>
                    <td><input type="text" name="email" value="{{ $customer->email }}" class="form-control" ></td>
                  </tr>

                  <tr>
                    <td>Nama Depan</td>
                    <td>:</td>
                    <td><input type="text" name="first_name" value="{{ $customer->first_name }}" class="form-control" ></td>
                  </tr>

                  <tr>
                    <td>Nama Belakang</td>
                    <td>:</td>
                    <td><input type="text" name="last_name" value="{{ $customer->last_name }}" class="form-control" ></td>
                  </tr>

                  <tr>
                    <td>Alamat</td>
                    <td>:</td>
                    <td><input type="text" name="address" value="{{ $customer->address }}" class="form-control" ></td>
                  </tr>

                  <tr>
                    <td>Nomor Telepon</td>
                    <td>:</td>
                    <td><input type="text" name="phone_number" value="{{ $customer->phone_number }}" class="form-control" ></td>
                  </tr>

                  <tr>
                    <td>Password</td>
                    <td>:</td>
                    <td><input type="text" name="password" value="{{ $customer->password }}" class="form-control" ></td>
                  </tr>

                  {{ csrf_field() }}
                  <input type="hidden" name="_method" value="PUT">

                </table>

                  <a href="/customer" type="submit" class="btn btn-primary">Kembali</a> &nbsp;&nbsp;
                  <button type="submit" class="btn btn-warning"><i class="fa fa-edit"></i> Edit Data Customer</button>
              </form>
            </div>
          </div>
      </div>

      <!-- Modal Pesan -->
      <div class="modal fade" id="pesan" role="dialog">
      	<div class="modal-dialog">
      		<div class="modal-content">
      				<div class="modal-header">
      						<button type="button" class="close" data-dismiss="modal">&times;</button>
      						<h4 class="modal-title">
      							<center>
                      @if (Session::has('sukses'))
                         <font color="green" size="4px"><b>{{ Session::get('sukses') }}</b></font>
                      @else
                         <font color="red" size="4px"><b>{{ Session::get('gagal') }}</b></font>
                      @endif

      							</center>
      						</h4>
      				</div>
      		</div>
      	</div>
      </div>

@endsection
